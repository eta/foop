//! OIDC login (because the library doesn't actually do this for you)

use anyhow::{anyhow, Context};
use openidconnect::core::{
    CoreAuthDisplay, CoreAuthenticationFlow, CoreClaimName, CoreClaimType, CoreClient,
    CoreClientAuthMethod, CoreGrantType, CoreJsonWebKey, CoreJsonWebKeyType, CoreJsonWebKeyUse,
    CoreJweContentEncryptionAlgorithm, CoreJweKeyManagementAlgorithm, CoreJwsSigningAlgorithm,
    CoreResponseMode, CoreResponseType, CoreSubjectIdentifierType,
};
use openidconnect::{
    AccessToken, AccessTokenHash, AdditionalProviderMetadata, AuthorizationCode, ClientId,
    ClientSecret, CsrfToken, IntrospectionUrl, IssuerUrl, Nonce, OAuth2TokenResponse,
    PkceCodeChallenge, PkceCodeVerifier, ProviderMetadata, RedirectUrl, TokenIntrospectionResponse,
    TokenResponse,
};
use rouille::{Request, Response};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::sync::{Arc, Mutex};

struct OidcState {
    verifier: PkceCodeVerifier,
    nonce: Nonce,
}

#[derive(Default)]
struct StateStoreInner {
    /// A map of OIDC 'state' parameters to the rest of the state required to complete
    /// a login flow.
    oidc_flows: HashMap<String, OidcState>,
}

pub struct OidcClient {
    state: Arc<Mutex<StateStoreInner>>,
    client: CoreClient,
}

impl OidcClient {
    const MAX_ELEMENTS: usize = 32;

    pub fn new(
        issuer_url: String,
        client_id: String,
        secret: String,
        redirect_uri: String,
    ) -> anyhow::Result<Self> {
        let provider_metadata = IntrospectedProviderMetadata::discover(
            &IssuerUrl::new(issuer_url).context("failed to parse OIDC issuer url")?,
            openidconnect::ureq::http_client,
        )
        .context("failed to autodiscover OIDC details")?;
        let introspection_uri = provider_metadata
            .additional_metadata()
            .introspection_endpoint
            .clone();
        let client = CoreClient::from_provider_metadata(
            provider_metadata,
            ClientId::new(client_id),
            Some(ClientSecret::new(secret)),
        )
        .set_introspection_uri(IntrospectionUrl::new(introspection_uri)?)
        .set_redirect_uri(
            RedirectUrl::new(redirect_uri).context("failed to make redirect uri, check baseurl")?,
        );

        Ok(Self {
            client,
            state: Arc::new(Mutex::new(Default::default())),
        })
    }

    pub fn generate_redirect_url(&self) -> anyhow::Result<String> {
        let (pkce_challenge, pkce_verifier) = PkceCodeChallenge::new_random_sha256();
        let (auth_url, csrf_token, nonce) = self
            .client
            .authorize_url(
                CoreAuthenticationFlow::AuthorizationCode,
                CsrfToken::new_random,
                Nonce::new_random,
            )
            .set_pkce_challenge(pkce_challenge)
            .url();

        self.store_flow(
            csrf_token,
            OidcState {
                verifier: pkce_verifier,
                nonce,
            },
        );
        Ok(auth_url.to_string())
    }

    pub fn handle_redirect(&self, request: &Request) -> anyhow::Result<Response> {
        let csrf_token = match request.get_param("state") {
            Some(v) => v,
            None => return Ok(Response::text("no state parameter").with_status_code(400)),
        };
        let code = match request.get_param("code") {
            Some(v) => v,
            None => return Ok(Response::text("no code parameter").with_status_code(400)),
        };
        let state = match self.retrieve_flow(&csrf_token) {
            Some(v) => v,
            None => return Ok(Response::text("invalid oidc state").with_status_code(400)),
        };
        println!("oidc: doing exchange for token {}", csrf_token);
        let token_response = self
            .client
            .exchange_code(AuthorizationCode::new(code))
            .set_pkce_verifier(state.verifier)
            .request(openidconnect::ureq::http_client)
            .context("failed to do token exchange")?;
        let id_token = token_response
            .id_token()
            .ok_or_else(|| anyhow!("server failed to return ID token"))?;
        let claims = id_token.claims(&self.client.id_token_verifier(), &state.nonce)?;
        if let Some(expected_access_token_hash) = claims.access_token_hash() {
            let actual_access_token_hash = AccessTokenHash::from_token(
                token_response.access_token(),
                &id_token.signing_alg()?,
            )?;
            if actual_access_token_hash != *expected_access_token_hash {
                return Err(anyhow!("Invalid access token"));
            }
        }
        let username = match claims.preferred_username() {
            Some(v) => v.as_str(),
            None => return Ok(Response::text("need a username set").with_status_code(400)),
        };
        println!(
            "oidc: new login: {} ({})",
            username,
            claims.subject().as_str()
        );
        let access_token = token_response.access_token().secret();
        let ret = Response::redirect_302("/").with_additional_header(
            "Set-Cookie",
            format!("token=\"{}\"; Secure; HttpOnly", access_token),
        );
        Ok(ret)
    }

    pub fn get_username(&self, request: &Request) -> Option<String> {
        let token =
            if let Some((_, val)) = rouille::input::cookies(request).find(|&(n, _)| n == "token") {
                val.to_string()
            } else {
                return None;
            };
        let introspection_response = match self
            .client
            .introspect(&AccessToken::new(token))
            .unwrap()
            .request(openidconnect::ureq::http_client)
        {
            Ok(v) => v,
            Err(e) => {
                eprintln!("oidc: introspection failed: {e}");
                return None;
            }
        };
        if !introspection_response.active() {
            println!("oidc: provided token is inactive, rejecting",);
            return None;
        }
        let sub = introspection_response.sub()?;
        let username = introspection_response.username()?;
        println!("oidc: request authenticated as {} ({})", username, sub);
        Some(username.into())
    }

    fn store_flow(&self, token: CsrfToken, state: OidcState) {
        let mut map = &mut self.state.lock().unwrap().oidc_flows;

        // Prevent fun DoS attacks where people start lots of OIDC auth flows but never complete
        // them. At least this way, you can DoS the login flow, but not consume all the RAM.
        while map.len() >= Self::MAX_ELEMENTS {
            eprintln!("oidc: state store is oversized, removing a key!");
            let random_key = map.keys().next().unwrap().clone();
            map.remove(&random_key);
        }

        println!("oidc: storing new flow state for token {}", token.secret());
        map.insert(token.secret().clone(), state);
    }

    fn retrieve_flow(&self, token: &str) -> Option<OidcState> {
        let ret = self.state.lock().unwrap().oidc_flows.remove(token);
        if ret.is_some() {
            println!("oidc: retrieved flow state for token {token}");
        }
        ret
    }
}

#[derive(Deserialize, Serialize, Clone, Debug)]
struct IntrospectionAdditionalMetadata {
    introspection_endpoint: String,
}

impl AdditionalProviderMetadata for IntrospectionAdditionalMetadata {}

type IntrospectedProviderMetadata = ProviderMetadata<
    IntrospectionAdditionalMetadata,
    CoreAuthDisplay,
    CoreClientAuthMethod,
    CoreClaimName,
    CoreClaimType,
    CoreGrantType,
    CoreJweContentEncryptionAlgorithm,
    CoreJweKeyManagementAlgorithm,
    CoreJwsSigningAlgorithm,
    CoreJsonWebKeyType,
    CoreJsonWebKeyUse,
    CoreJsonWebKey,
    CoreResponseMode,
    CoreResponseType,
    CoreSubjectIdentifierType,
>;
