//! Configuration file.

use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct OidcConfig {
    pub client_id: String,
    pub secret: String,
    pub issuer: String,
}

#[derive(Deserialize)]
pub struct Config {
    pub oidc: OidcConfig,
    pub storage_dir: String,
    pub baseurl: String,
    pub listen: String,
}
