use crate::config::Config;
use crate::oidc::OidcClient;
use anyhow::Context;
use rouille::{router, Response};

mod config;
mod oidc;

fn main() -> anyhow::Result<()> {
    let config_location = std::env::args()
        .nth(1)
        .unwrap_or_else(|| "./foop.toml".into());
    println!("loading config at {config_location}...");

    let config: Config = toml::from_str(&std::fs::read_to_string(&config_location)?)?;

    println!(
        "creating directory at '{}' for file storage...",
        config.storage_dir
    );
    std::fs::create_dir_all(config.storage_dir).context("could not create storage_dir")?;

    println!("setting up OIDC provider...");
    let oidc = OidcClient::new(
        config.oidc.issuer,
        config.oidc.client_id,
        config.oidc.secret,
        format!("{}/auth", config.baseurl),
    )?;
    println!("starting server on '{}'...", config.listen);

    rouille::start_server(&config.listen, move |request| {
        router!(request,
            (GET) (/) => {
                let username = oidc.get_username(request);
                match username {
                    Some(v) => Response::text(format!("hello, {}!", v)),
                    None => Response::text("not logged in")
                }
            },
            (GET) (/login) => {
                match oidc.generate_redirect_url() {
                    Ok(v) => Response::redirect_302(v),
                    Err(e) => {
                        eprintln!("failed to generate OIDC redirect! {e}");
                        Response::text("whoops, looks like I'm broken")
                            .with_status_code(500)
                    }
                }
            },
            (GET) (/auth) => {
                match oidc.handle_redirect(request) {
                    Ok(v) => v,
                    Err(e) => {
                        eprintln!("failed to handle OIDC redirect! {e}");
                        Response::text("whoops, looks like I'm broken")
                            .with_status_code(500)
                    }
                }
            },
            _ => {
                Response::empty_404()
            }
        )
    });
}
